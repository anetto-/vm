"""
pyinfra
"""
import pyinfra
from pyinfra.operations import apt, server
import read_config



SUDO = True
USE_SUDO_PASSWORD = True
STATE = pyinfra.api.State()

PATH = "settings_env.ini"

packages = read_config.get_install_apt(PATH)
for package in packages:
    if packages[package] == "latest":
        apt.packages(
            name=f'Install latest {package}',
            packages=package,
            latest=True,
            state=STATE
        )
    else:
        apt.packages(
            name=f'Install {package} version {packages[package]}',
            packages=f"{package}={packages[package]}",
            state=STATE
        )

try:
    COMMANDS = read_config.get_others_comands(PATH)
    for command in COMMANDS:
        server.shell(
            {f'do {command}'},  # Use a set as the first argument to name the operation
            COMMANDS[command],
        )
except KeyError:
    # just skipping
    pass

try:
    USERS = read_config.get_users(PATH)
    for user in USERS:
        server.user(
            name=f'Creating user {user}',
            user=user,
            present=True,
            home=None,
            shell=None,
            group=None,
            groups=None,
            public_keys=None,
            delete_keys=False,
            ensure_home=True,
            system=False,
            uid=None,
            comment=None,
            add_deploy_dir=True,
            unique=True
        )
except KeyError:
    # just skipping
    pass
