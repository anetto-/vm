from pyinfra.api import host
from pyinfra.operations import apt, server, pip
from pyinfra_docker import deploy_docker
import read_config
import pyinfra

SUDO = True
use_sudo_password = True
state = pyinfra.api.State()

path = "settings.ini"

keys_rep = read_config.get_keys_rep(path)
for key_rep in keys_rep:
    apt.key(
        name=f'Add the {key_rep} apt gpg key',
        src=keys_rep[key_rep],
    )

repositorys = read_config.get_repo(path)
for repository in repositorys:
    apt.repo(
        name=f'Install {repository} repo',
        src=repositorys[repository],
    )

ppas = read_config.get_add_ppa(path)
for ppa in ppas:
    apt.ppa(
        {f'Add the {ppa} ppa'},
        ppas[ppa],
    )

update_systems = read_config.get_update_system(path)
for item in update_systems:
    if item == 'update' and update_systems[item] == "Yes":
        apt.update(
            name='Update apt repositories'
        )
    if item == 'upgrade' and update_systems[item] == "Yes":
        apt.upgrade(
            name='Upgrade apt packages',
        )

packages = read_config.get_install_apt(path)
for package in packages:
    if packages[package] == "latest":
        apt.packages(
            name=f'Install latest {package}',
            packages=package,
            latest=True,
            state=state
        )
    else:
        apt.packages(
            name=f'Install {package} version {packages[package]}',
            packages=f"{package}={packages[package]}",
            state=state
        )

install_docker = read_config.get_install_docker(path)
for item in install_docker:
    if item == "install_docker" and install_docker[item] == 'Yes':
        deploy_docker()
    elif item == 'install_docker_compose' and install_docker[item] == 'Yes':
        server.shell(
            {'Download docker-compose'},  # Use a set as the first argument to name the operation
            'curl -L "https://github.com/docker/compose/releases/download/1.25.3/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose',
        )
        server.shell(
            {'Chmod docker-compose'},
            'chmod +x /usr/local/bin/docker-compose'
        )

packages = read_config.get_install_apt2(path)
for package in packages:
    if packages[package] == "latest":
        apt.packages(
            name=f'Install latest {package}',
            packages=package,
            latest=True,
            state=state
        )
    else:
        apt.packages(
            name=f'Install {package} version {packages[package]}',
            packages=f"{package}={packages[package]}",
            state=state
        )

pips = read_config.get_install_pip3(path)
for pip_package in pips:
    if pips[pip_package] == "latest":
        pip.packages(
            {f'Install {pip_package} in pip3'},
            pip_package,
            pip='pip3'
        )
    else:
        pip.packages(
            {f'Install {pip_package} version {pips[pip_package]} in pip3'},
            f'{pip_package}={pips[pip_package]}',
            pip='pip3'
        )

dockers = read_config.get_pull_docker(path)
for docker in dockers:
    if dockers[docker] == "latest":
        server.shell(
            {f'docker pull {docker}:latest'},  # Use a set as the first argument to name the operation
            f'docker pull {docker}:latest',
        )
    else:
        server.shell(
            {f'docker pull {docker}:{dockers[docker]}'},  # Use a set as the first argument to name the operation
            f'docker pull {docker}:{dockers[docker]}',
        )

snaps = read_config.get_install_snap(path)
for snap in snaps:
    if snaps[snap] == "latest":
        server.shell(
            {f'snap install {snap}'},  # Use a set as the first argument to name the operation
            f'snap install {snap}',
        )
    else:
        server.shell(
            {f'snap install {snap} version {snaps[snap]}'},  # Use a set as the first argument to name the operation
            f'snap install {snap}={snaps[snap]}',
        )

commands = read_config.get_others_comands(path)
for command in commands:
    server.shell(
        {f'do {command}'},  # Use a set as the first argument to name the operation
        commands[command],
    )

users = read_config.get_users(path)
for user in users:
    server.user(
        name=f'Creating user {user}',
        user=user,
        present=True,
        home=None,
        shell=None,
        group=None,
        groups=None,
        public_keys=None,
        delete_keys=False,
        ensure_home=True,
        system=False,
        uid=None,
        comment=None,
        add_deploy_dir=True,
        unique=True
    )
