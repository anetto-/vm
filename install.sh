#!/bin/bash
apt install python3-pip
pip install pyinfra
pip install pyinfra-docker
pyinfra @local install_env.py
pyinfra @local install_packages.py
pyinfra @local install_ide.py
