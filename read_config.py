import configparser

def get_install_apt(path):
    config = configparser.ConfigParser()
    config.read(path)
    return config._sections["install_apt"]

def get_update_system(path):
    config = configparser.ConfigParser()
    config.read(path)
    return config._sections["update"]

def get_repo(path):
    config = configparser.ConfigParser()
    config.read(path)
    return config._sections["add_repository"]

def get_install_docker(path):
    config = configparser.ConfigParser()
    config.read(path)
    return config._sections["Docker"]

def get_add_ppa(path):
    config = configparser.ConfigParser()
    config.read(path)
    return config._sections["add_ppa"]

def get_pull_docker(path):
    config = configparser.ConfigParser()
    config.read(path)
    return config._sections["docker_pull"]

def get_install_snap(path):
    config = configparser.ConfigParser()
    config.read(path)
    return config._sections["install_snap"]

def get_install_pip3(path):
    config = configparser.ConfigParser()
    config.read(path)
    return config._sections["install_pip3"]

def get_others_comands(path):
    config = configparser.ConfigParser()
    config.read(path)
    return config._sections["others"]

def get_keys_rep(path):
    config = configparser.ConfigParser()
    config.read(path)
    return config._sections["add_key_repository"]
    
def get_install_apt2(path):
    config = configparser.ConfigParser()
    config.read(path)
    return config._sections["install_apt2"]

def get_users(path):
    config = configparser.ConfigParser()
    config.read(path)
    return config._sections["users"]

def get_deb(path):
    config = configparser.ConfigParser()
    config.read(path)
    return config._sections["install_deb"]
