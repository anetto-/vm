from pyinfra.operations import server
import pyinfra
import configparser

SUDO = True
use_sudo_password = True
state = pyinfra.api.State()

path = "settings.ini"
#PATH = "settings_ide.ini"


def get_install_jetbrains():
    """
    Парсит чтобы возвращать директории в которой находится архив с ide
    """
    config = configparser.ConfigParser()
    config.read(path)
    return config._sections["add_directory_jetbrains"]

if __name__ == 'builtins':
    archives = get_install_jetbrains()
    for archive in archives:
        index_to_fullname = archives[archive].rfind('/')
        name_product_fullname = archives[archive][index_to_fullname + 1:]
        index_to_sh = name_product_fullname.find('-')
        name_product_to_sh = name_product_fullname[:index_to_sh]
        server.shell(
            {f'extract {name_product_fullname}'},
            f'tar xvzf {archives[archive]} -C /opt/',
        )
        server.shell(
            {f'install {name_product_fullname}'},
            f'sh /opt/{name_product_fullname[:-7].lower()}/bin/{name_product_to_sh.lower()}.sh',
        )
