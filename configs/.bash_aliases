#!/bin/bash
# author : an
# version : v1.0
# date : 2016_05_18

#alias gl='git log --graph --pretty=format:"%t, %ar: %s"'
alias gl="git log --graph --pretty=format:'%C(yellow)%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit --date=short "
alias githist='git log --pretty=format:"%h %ad | %s%d [%an]" --graph --date=short'
alias go='git checkout'
alias gd='git diff'
alias gs='git status'
alias gitb='git branch -v'
alias gitcam='git commit -a -m'
alias gitc='git commit -m'

alias tm='tmux attach || tmux new'

alias TAR='tar -zcvf' # dest source
alias UNTAR='tar -zxvf' # source

# short pylint report
alias pylint="pylint -rn"
#alias pylint="pylint -rn --output-format=colorized"
alias pylintall="pylint -rn *.py | grep -v 'Locally ' "

alias ..="cd .."
alias ...="cd ../.."
alias ....="cd ../../.."
alias .....="cd ../../../.."

# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

alias h="history | tail -30"

alias bash-reload="source ~/.bashrc"

################################
# suppz
################################
#export PATH=/common/runmvs/bin:$PATH
alias grunmvs='sudo vim /usr/runmvs/.grunmvs'
alias sched1='sudo vim /usr/runmvs/qserv/src/sched/qqset.hc'
alias sched2='cd /usr/runmvs/qserv/src/; sudo bash mksched'
alias sched3='cd -; chshed'

################################
# work PC
################################
# fix microphone on work PC
alias MIC='amixer set Headphone 80% unmute || echo ""'
