"""
pyinfra
"""
from pyinfra.api import host
from pyinfra.operations import apt, server, pip
import pyinfra
from pyinfra_docker import deploy_docker
import read_config


SUDO = True
USE_SUDO_PASSWORD = True
STATE = pyinfra.api.State()

PATH = "settings_pkg.ini"

KEYS_REP = read_config.get_keys_rep(PATH)
for key_rep in KEYS_REP:
    apt.key(
        name=f'Add the {key_rep} apt gpg key',
        src=KEYS_REP[key_rep],
    )

REPOSITORYS = read_config.get_repo(PATH)
for repository in REPOSITORYS:
    apt.repo(
        name=f'Install {repository} repo',
        src=REPOSITORYS[repository],
    )

PPAS = read_config.get_add_ppa(PATH)
for ppa in PPAS:
    apt.ppa(
        name=f'Add the {ppa} ppa',
        src=PPAS[ppa],
    )

UPDATE_SYSTEMS = read_config.get_update_system(PATH)
for item in UPDATE_SYSTEMS:
    if item == 'update' and UPDATE_SYSTEMS[item] == "Yes":
        apt.update(
            name='Update apt repositories'
        )
    if item == 'upgrade' and UPDATE_SYSTEMS[item] == "Yes":
        apt.upgrade(
            name='Upgrade apt packages',
        )

PACKAGES = read_config.get_install_apt(PATH)
for package in PACKAGES:
    if PACKAGES[package] == "latest":
        apt.packages(
            name=f'Install latest {package}',
            packages=package,
            latest=True,
            state=STATE
        )
    else:
        apt.packages(
            name=f'Install {package} version {PACKAGES[package]}',
            packages=f"{package}={PACKAGES[package]}",
            state=STATE
        )

INSTALL_DOCKER = read_config.get_install_docker(PATH)
for item in INSTALL_DOCKER:
    if item == "INSTALL_DOCKER" and INSTALL_DOCKER[item] == 'Yes':
        deploy_docker()
    elif item == 'INSTALL_DOCKER_compose' and INSTALL_DOCKER[item] == 'Yes':
        server.shell(
            name='Download DOCKER-compose',  # Use a set as the first argument to name the operation
            commands='curl -L "https://github.com/DOCKER/compose/releases/download/1.25.3/DOCKER-compose-$(uname -s)-$(uname -m)" ''-o /usr/local/bin/DOCKER-compose',
        )
        server.shell(
            name='Chmod DOCKER-compose',
            commands='chmod +x /usr/local/bin/DOCKER-compose'
        )

"""
PACKAGES = read_config.get_install_apt2(PATH)
for package in PACKAGES:
    if PACKAGES[package] == "latest":
        apt.packages(
            name=f'Install latest {package}',
            packages=package,
            latest=True,
            state=STATE
        )
    else:
        apt.packages(
            name=f'Install {package} version {PACKAGES[package]}',
            packages=f"{package}={PACKAGES[package]}",
            state=STATE
        )
"""

PIPS = read_config.get_install_pip3(PATH)
for pip_package in PIPS:
    if PIPS[pip_package] == "latest":
        pip.packages(
            {f'Install {pip_package} in pip3'},
            pip_package,
            pip='pip3'
        )
    else:
        pip.packages(
            {f'Install {pip_package} version {PIPS[pip_package]} in pip3'},
            f'{pip_package}={PIPS[pip_package]}',
            pip='pip3'
        )

DOCKERS = read_config.get_pull_docker(PATH)
for docker in DOCKERS:
    if DOCKERS[docker] == "latest":
        server.shell(
            name=f'docker pull {docker}:latest',  # Use a set as the first argument to name the operation
            commands=f'docker pull {docker}:latest',
        )
    else:
        server.shell(
            name=f'docker pull {docker}:{DOCKERS[docker]}',  # Use a set as the first argument to name the operation
            commands=f'docker pull {docker}:{DOCKERS[docker]}',
        )

SNAPS = read_config.get_install_snap(PATH)
for snap in SNAPS:
    if SNAPS[snap] == "latest":
        server.shell(
            name=f'snap install {snap}',  # Use a set as the first argument to name the operation
            commands=f'snap install {snap}',
        )
    else:
        server.shell(
            name=f'snap install {snap} version {SNAPS[snap]}',  # Use a set as the first argument to name the operation
            commands=f'snap install {snap}={SNAPS[snap]}',
        )

try:
    DEBS = read_config.get_deb(PATH)
    for deb in DEBS:
        server.shell(
            name=f'dpkg -i {deb}',
            commands=f'dpkg -i {deb}.deb',
        )
except KeyError:
    # just skipping
    pass
